/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;



import javax.swing.JToggleButton;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;


import org.opencv.videoio.VideoCapture;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;


/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */


 






public class Robot extends IterativeRobot {
//Declare ALL Vars Here!


    //This is where all compressors are defined
  Compressor _compressedor = new Compressor(2);

     //This is were all Joysticks are defined

     Joystick _joystick = new Joystick(0);

      //This is where all Solenoids are defined

     Solenoid _solenoid1_1 = new Solenoid(2, 0);

          //This is where all the motors are defined

               CANSparkMax _leftBackCanSparkMax = new CANSparkMax((15), MotorType.kBrushless);

               CANSparkMax _leftFrontCanSparkMax = new CANSparkMax((14), MotorType.kBrushless);

             CANSparkMax _rightBackCanSparkMax = new CANSparkMax((12), MotorType.kBrushless);
        
              CANSparkMax _rightFrontCanSparkMax = new CANSparkMax((13), MotorType.kBrushless);

              Boolean isToggled = false;


              private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();

  private SpeedControllerGroup m_LeftMotors = new SpeedControllerGroup(_leftBackCanSparkMax,_leftFrontCanSparkMax);
  private SpeedControllerGroup m_RightMotors = new SpeedControllerGroup(_rightBackCanSparkMax,_rightFrontCanSparkMax);
  private DifferentialDrive m_Drive = new DifferentialDrive(m_LeftMotors,m_RightMotors);


  private boolean m_LimelightHasValidTarget = false;
  private double m_LimelightDriveCommand = 0.0;
  private double m_LimelightSteerCommand = 0.0;
  double _tavar = 0.0;


  @Override
  public void robotInit() {
   
    _compressedor.setClosedLoopControl(false);

  


        


 _leftBackCanSparkMax.set(0);

_leftFrontCanSparkMax.set(0);

 _rightBackCanSparkMax.set(0);

 _rightFrontCanSparkMax.set(0);








 m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
 m_chooser.addOption("My Auto", kCustomAuto);
 SmartDashboard.putData("Auto choices", m_chooser);


      

      




    }

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {

    
//read values periodically




            
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
   
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
  
   
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopInit() {

    

   
          

 
    
      

    

      

      
    


  }


  @Override
  public void teleopPeriodic() {
    double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);

    System.out.println(tx);

    







// if (joystick->GetRawButton(9))
// {
//         float heading_error = tx;
//         steering_adjust = Kp * tx;

//         left_command+=steering_adjust;
//         right_command-=steering_adjust;
// }







          double _joyforward = 1 * _joystick.getY();
          double _joyrotate = 1 * _joystick.getZ();


if (!_joystick.getRawButton(1)) { 
          				
		if (_joyforward >  -0.25 && _joyforward < 0.25) { 
      _leftFrontCanSparkMax.set((_joyrotate )/5 );
      _rightFrontCanSparkMax.set((_joyrotate)/5 );
      _leftBackCanSparkMax.set((_joyrotate)/5 );
      _rightBackCanSparkMax.set((_joyrotate )/5 );
      

    }

    else if (_joyrotate > -0.25 && _joyrotate < 0.25) {
      _leftFrontCanSparkMax.set((_joyforward + 0.25)/4);
      _rightFrontCanSparkMax.set((-_joyforward - 0.25)/4);
      _leftBackCanSparkMax.set((_joyforward + 0.25)/4);
      _rightBackCanSparkMax.set((-_joyforward - 0.25)/4);
   
    }

    else {
      _leftFrontCanSparkMax.set(0);                                                            
      _rightFrontCanSparkMax.set(0);
      _leftBackCanSparkMax.set(0);
      _rightBackCanSparkMax.set(0);   

    }

  }

  else if (_joystick.getRawButton(1)) { 
          				
		if (_joyforward >  -0.25 && _joyforward < 0.25) { 
      _leftFrontCanSparkMax.set((_joyrotate ));
      _rightFrontCanSparkMax.set((_joyrotate));
      _leftBackCanSparkMax.set((_joyrotate) );
      _rightBackCanSparkMax.set((_joyrotate ) );
      

    }

    else if (_joyrotate > -0.25 && _joyrotate < 0.25) {
      _leftFrontCanSparkMax.set((_joyforward + 0.25));
      _rightFrontCanSparkMax.set((-_joyforward - 0.25));
      _leftBackCanSparkMax.set((_joyforward + 0.25));
      _rightBackCanSparkMax.set((-_joyforward - 0.25));
   
    }

    else {
      _leftFrontCanSparkMax.set(0);                                                            
      _rightFrontCanSparkMax.set(0);
      _leftBackCanSparkMax.set(0);
      _rightBackCanSparkMax.set(0);   

    }

  }

  else {
    _leftFrontCanSparkMax.set(0);                                                            
    _rightFrontCanSparkMax.set(0);
    _leftBackCanSparkMax.set(0);
    _rightBackCanSparkMax.set(0);   

  }

 

        


  
    if (_joystick.getRawAxis(3) < -0.8) {

        fCompOn();

  }

  else {

      fCompOff();

  }



  
  
  
  if (_joystick.getRawButtonPressed(2) == true) {
    //If joystick is up

        

    if (isToggled == true) {
          isToggled = false;
          fSolenoidRe();
          

    }
    else if (isToggled == false){
        isToggled = true;
        fSolenoidEx();
    }

    
    
    

  }


  // else if (_joystick.getY() > 0.25) {

  //     //If Joystick is down
     
  //     fSolenoidRe();
      


  // }


  


  }
  



  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
    
    
    
      
      Update_Limelight_Tracking();

      double steer = _joystick.getZ();
      double drive = -_joystick.getY();
      boolean auto = _joystick.getRawButton(9);

    System.out.println(m_LimelightHasValidTarget);

      steer *= 1;
      drive *= -1;

      if (auto)
      {
        if (m_LimelightHasValidTarget)
        {
              m_Drive.arcadeDrive(m_LimelightDriveCommand, m_LimelightSteerCommand);
        }

        else if (_tavar > 3.0) {
          _leftFrontCanSparkMax.set(-0.3);                                                            
          _rightFrontCanSparkMax.set(-0.3);
          _leftBackCanSparkMax.set(-0.3);
          _rightBackCanSparkMax.set(-0.3);   



        }
        else
        {
              m_Drive.arcadeDrive(0.0,0.0);
        }

        
      }
      else
      {
        m_Drive.arcadeDrive(drive/2,steer/2);
      }       





                }
                    




        

      
       


    

  

  //This is our custom functions

  public void fCompOn() {

    _compressedor.setClosedLoopControl(true);
    _compressedor.start();
//  System.out.println("Compressor On");

  }

  public void fCompOff() {

    _compressedor.setClosedLoopControl(false);
     _compressedor.stop();
    // System.out.println("Compressor Off");

  }

  public void fSolenoidRe() {

    System.out.println("Retracting Solenoid");
    _solenoid1_1.set(false);
  }

  public void fSolenoidEx() {

    System.out.println("Extending Solenoid");
    _solenoid1_1.set(true);

  }







public void Update_Limelight_Tracking()
{
      // These numbers must be tuned for your Robot!  Be careful!
      final double STEER_K = 0.05;                    // how hard to turn toward the target
      final double DRIVE_K = 0.26;                    // how hard to drive fwd toward the target
      final double DESIRED_TARGET_AREA = 13.0;        // Area of the target when the robot reaches the wall
      final double MAX_DRIVE = 0.7;                   // Simple speed limit so we don't drive too fast

      double tv = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
      double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
      double ty = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0);
      double ta = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ta").getDouble(0);

     //   ty = ty - 8.00;
     ta = ta - 13.00;

      if (tv != 1.0)
      {
        m_LimelightHasValidTarget = false;
        m_LimelightDriveCommand = 0.0;
        m_LimelightSteerCommand = 0.0;
        return;
      }
      else if (tv == 1.0) {

      

      m_LimelightHasValidTarget = true;

      }

      // Start with proportional steering
      double steer_cmd = tx * STEER_K;
      m_LimelightSteerCommand = steer_cmd;

      // try to drive forward until the target area reaches our desired area
      //double drive_cmd = (DESIRED_TARGET_AREA - ta) * DRIVE_K;
double drive_cmd = ta * DRIVE_K;

      // don't let the robot drive too fast into the goal
     // if (drive_cmd > MAX_DRIVE)
     // {
     //   drive_cmd = MAX_DRIVE;
     // }
     m_LimelightDriveCommand = drive_cmd/5;

     _tavar = ta;
}
}




