/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;



import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Solenoid;
import io.github.pseudoresonance.pixy2api.Pixy2;
import io.github.pseudoresonance.pixy2api.Pixy2.LinkType;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */


 






public class Robot extends IterativeRobot {
//Declare ALL Vars Here!
  Compressor _compressedor = new Compressor(2);

     Joystick _joystick = new Joystick(0);

     Solenoid _solenoid1_1 = new Solenoid(2, 0);

     

     //Pixy2 _pixyCam = Pixy2.createInstance(LinkType.UART);

    // Pixy2 _pixyCam = Pixy2.createInstance(LinkType.UART);

     

     //_pixyCam.init();

     

    

     //_pixyCam.init(0);
            
  //  _pixyCam.init();

     
 

  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
   
    _compressedor.setClosedLoopControl(false);

//CameraServer.getInstance().startAutomaticCapture();

      //_pixyCam.init();
  
  }

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {


            
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    // m_autoSelected = m_chooser.getSelected();
    // // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    // System.out.println("Auto selected: " + m_autoSelected);
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    
    
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopInit() {

    

   // _pixyCam.getLine();
          

 
    
      

    

      

      
    


  }


  @Override
  public void teleopPeriodic() {

        


  
    if (_joystick.getRawAxis(3) < 0.05) {

        fCompOn();

  }

  else {

      fCompOff();

  }



  
  if (_joystick.getY() < -0.25) {
    //If joystick is up
    
    fSolenoidEx();
    

  }


  else if (_joystick.getY() > 0.25) {

      //If Joystick is down
     
      fSolenoidRe();
      


  }


  


  }
  



  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {

   

  }

  //This is our custom functions

  public void fCompOn() {

    _compressedor.setClosedLoopControl(true);
    _compressedor.start();
 System.out.println("Compressor On");

  }

  public void fCompOff() {

    _compressedor.setClosedLoopControl(false);
    _compressedor.stop();
    System.out.println("Compressor Off");

  }

  public void fSolenoidRe() {

    System.out.println("Retracting Solenoid");
    _solenoid1_1.set(false);
  }

  public void fSolenoidEx() {

    System.out.println("Extending Solenoid");
    _solenoid1_1.set(true);

  }


}
