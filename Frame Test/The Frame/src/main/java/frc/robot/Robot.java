
    package frc.robot;

                import edu.wpi.first.wpilibj.Joystick;
                import edu.wpi.first.wpilibj.TimedRobot;
                import com.ctre.phoenix.motorcontrol.NeutralMode;
                import com.ctre.phoenix.motorcontrol.ControlMode;
                import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
                import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.IterativeRobot;
                import edu.wpi.first.wpilibj.Joystick;
                import edu.wpi.first.wpilibj.PowerDistributionPanel;
                import edu.wpi.first.wpilibj.TimedRobot;
                import edu.wpi.first.wpilibj.buttons.JoystickButton;




public class Robot extends TimedRobot{
    //If we want gobal variables (AKA Vars that an method can use, we put it here.)

Joystick _joystick1;

            TalonSRX _leftBack = new TalonSRX(9);
            //	TalonSRX _leftBottomSlave = new TalonSRX(17);
        
            double leftBackpwr;


            TalonSRX _leftFront = new TalonSRX(2);

            double leftFrontpwr;

            //	TalonSRX _leftTopSlave = new TalonSRX(15);

            TalonSRX _rightBack = new TalonSRX(13);

            double rightBackpwr;

            //	TalonSRX _rightBottomSlave = new TalonSRX(16);
                
            TalonSRX _rightFront = new TalonSRX(17);

            double rightFrontpwr;
            //	TalonSRX _rightTopSlave = new TalonSRX(14);

            //  Encoder leftEnc = new Encoder(0, 1, false, Encoder.EncodingType.k4X);

            //  Encoder rightEnc = new Encoder(0, 1, false, Encoder.EncodingType.k4X);




//Drive drive;


        @Override
        public void robotInit(){

            // This is where all variables are assigned, for neatness sake, and this is where we check for 
            // input and react to it, I think.


            CameraServer.getInstance().startAutomaticCapture();


        }
            @Override
            public void teleopInit(){


            _joystick1 = new Joystick(0);


            _leftBack.set(ControlMode.Position,  leftBackpwr);

                _leftBack.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder);

                  //  _leftBack.configurePID(25);
                    

            _rightBack.set(ControlMode.Position,  rightBackpwr);

                  _rightBack.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder);



            // leftEnc.setMaxPeriod(.1);
            // leftEnc.setMinRate(10);
            // leftEnc.setDistancePerPulse(5);
            // leftEnc.setReverseDirection(true);
            // leftEnc.setSamplesToAverage(7);
            


            // rightEnc.setMaxPeriod(.1);
            // rightEnc.setMinRate(10);
            // rightEnc.setDistancePerPulse(5);
            // rightEnc.setReverseDirection(false);
            // rightEnc.setSamplesToAverage(7);
            
                  
          // React

                if (_joystick1.getRawButton(2)) {




                }

                if (_joystick1.getX() > 0.25 || _joystick1.getX() < -0.25 || _joystick1.getY() > 0.25 || _joystick1.getY() < -0.25 || _joystick1.getZ() > 0.25 || _joystick1.getZ() < -0.25) {

                        moveRobot();

                }

                            // This is the timer for encoder ouput
                            // int enctim = 0;
                            // if (enctim == 5000) {
                            //     System.out.println("______________________________________________");
                            //     System.out.println("Left Encoder Position:" + leftEnc.getDistance());
                            //     System.out.println("Right Encoder Positiion:" + rightEnc.getDistance());
                            //     System.out.println("----------------------------------------------");
                            //     enctim = 0;
                            // }
                            // else {
                            //     enctim = enctim + 1;
                            // }





                


        }









            //Here is the deadband setting for the controller

            
                
            // @Override
        	// public void teleopPeriodic() {	

            
            // }



            
                //This is all for the movement of the stick

                double forward = -1 * _joystick1.getY();
                     double rotate = 1 * _joystick1.getZ();
                            double gameX = _joystick1.getX();
                              double turn = _joystick1.getTwist();	
                                double throttle = _joystick1.getThrottle();
                
                                //This is for the stick buttons

                                boolean topButton = _joystick1.getTop();
                                        boolean triggerButton = _joystick1.getTrigger();




        //This is where the functions will go (move, blink etc)

         public void moveRobot() {

            // if (motorsOn == false) {
            //     _leftFront.set(ControlMode.PercentOutput, 0);
            //     _rightFront.set(ControlMode.PercentOutput,0);
            //     _leftBack.set(ControlMode.PercentOutput, 0);
            //     _rightBack.set(ControlMode.PercentOutput, 0);
                
                
            // }

            if (rotate > 0.25 || rotate < -0.25) {
												
                forward = 0;
            }
            
            if (forward > 0.50 || forward < -0.75) {
                
                rotate = 0;
            }


            if (forward >  -0.25 && forward < 0.25) { 
                _leftFront.set(ControlMode.PercentOutput, -rotate/3);
                _rightFront.set(ControlMode.PercentOutput, -rotate/3);
                _leftBack.set(ControlMode.PercentOutput, -rotate/3);
                _rightBack.set(ControlMode.PercentOutput, -rotate/3);
                
     }
    
    else if (rotate > -0.25 && rotate < 0.025) {
        _leftFront.set(ControlMode.PercentOutput, (forward + 0.25)/2);
        _rightFront.set(ControlMode.PercentOutput, (-forward - 0.25)/2);
        _leftBack.set(ControlMode.PercentOutput, (forward + 0.25)/2);
        _rightBack.set(ControlMode.PercentOutput, (-forward - 0.25)/2);
        
        
    }
    
    
    
    
    else {
        _leftFront.set(ControlMode.PercentOutput, 0);
        _rightFront.set(ControlMode.PercentOutput,0);
        _leftBack.set(ControlMode.PercentOutput, 0);
        _rightBack.set(ControlMode.PercentOutput, 0);
        
    }

            



        }




}