/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import javax.swing.JToggleButton;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import org.opencv.videoio.VideoCapture;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */

public class Robot extends IterativeRobot {
  // Declare ALL Vars Here!

  // This is where all compressors are defined
  Compressor _compressedor = new Compressor(2);

  // This is were all Joysticks are defined

  Joystick _joystick = new Joystick(0);

  // This is where all Solenoids are defined

  Solenoid _solefront = new Solenoid(2, 4);

  Solenoid _soleback = new Solenoid(2, 3);
  // 2 grab 3 extend

  Solenoid _hatchGrabb2 = new Solenoid(2, 7);

  Solenoid _hatchExt3 = new Solenoid(2, 0);

  //The New Grabber
  // This is where all the motors are defined
  // E g g
  CANSparkMax _leftBackCanSparkMax = new CANSparkMax((13), MotorType.kBrushless);

  CANSparkMax _leftFrontCanSparkMax = new CANSparkMax((14), MotorType.kBrushless);

  CANSparkMax _rightBackCanSparkMax = new CANSparkMax((12), MotorType.kBrushless);

  CANSparkMax _rightFrontCanSparkMax = new CANSparkMax((11), MotorType.kBrushless);

  // TalonSRX _lifter = new TalonSRX(0);
    TalonSRX  _armLifter = new TalonSRX(3);
    TalonSRX _ballGrabber = new TalonSRX(17);

  Boolean isToggled = false;

  Boolean launchingFinal = false;

  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();

  private SpeedControllerGroup m_LeftMotors = new SpeedControllerGroup(_leftBackCanSparkMax, _leftFrontCanSparkMax);
  private SpeedControllerGroup m_RightMotors = new SpeedControllerGroup(_rightBackCanSparkMax, _rightFrontCanSparkMax);
  private DifferentialDrive m_Drive = new DifferentialDrive(m_LeftMotors, m_RightMotors);

  private boolean m_LimelightHasValidTarget = false;
  private double m_LimelightDriveCommand = 0.0;
  private double m_LimelightSteerCommand = 0.0;
  double _tavar = 0.0;

  double _joyforward = 0.0;
  double _joyrotate = 0.0;

  Boolean directionFlipped = false;
  Boolean button12Toggle = false;
  Boolean button11Toggle = false;
  Boolean speedToggle = false;
  Boolean grabMay = false;
  Boolean middleSole = false;

  Boolean frontSoleAct = false;
  Boolean backSoleAct = false;

  Timer timerRobo = new Timer();

  NetworkTableEntry ledEntry;
  NetworkTableEntry camMode;
  
  // @Override
  // public void disabledPeriodic() {
  //   _hatchGrabb2.set(true);
  // }

  @Override
  public void robotInit() {
    CameraServer.getInstance().startAutomaticCapture();
    _compressedor.setClosedLoopControl(false);

    fCompOn();

    _leftBackCanSparkMax.set(0);

    _leftFrontCanSparkMax.set(0);

    _rightBackCanSparkMax.set(0);

    _rightFrontCanSparkMax.set(0);

    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);


  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for
   * items like diagnostics that you want ran during disabled, autonomous,
   * teleoperated and test.
   *
   * <p>
   * This runs after the mode specific periodic functions, but before LiveWindow
   * and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {

    // read values periodically

  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable chooser
   * code works with the Java SmartDashboard. If you prefer the LabVIEW Dashboard,
   * remove all of the chooser code and uncomment the getString line to get the
   * auto name from the text box below the Gyro
   *
   * <p>
   * You can add additional auto modes by adding additional comparisons to the
   * switch structure below with additional strings. If using the SendableChooser
   * make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {

    // Update_Limelight_Tracking();
    // ledEntry.setDouble(1);
    // camMode.setDouble(1);
    teleopInit();
  
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    teleopPeriodic();
    //limelightAutonomous();
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopInit() {

    directionFlipped = false;

    Update_Limelight_Tracking();

    ledEntry.setDouble(1);

    camMode.setDouble(1);

    
  }

  @Override
  public void teleopPeriodic() {
    double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);

    double _joyforwardRaw = _joystick.getY();
    double _joyrotateRaw = _joystick.getZ();

    // Basic thing is we are actuating them sepretly.
    // if (_joystick.getRawAxis(3) < -0.8) {
//       if (_joystick.getRawButtonPressed(6)) {  

//         if(frontSoleAct == false) {
//         // Activate Front Hydrolics
//         _solefront.set(true);
//           frontSoleAct = true;
//       } else if (frontSoleAct == true) {
//         _solefront.set(false);
//         frontSoleAct = false;
//       }
  
  
//     }
    
  
  
//   if (_joystick.getRawButtonPressed(4)) {
        
//     if(backSoleAct == false) {
//    // Back Riders
//         _soleback.set(true);
//     backSoleAct = true;

//   }
  
  
//   else if (backSoleAct == true) {
// _soleback.set(false);
// backSoleAct = false;
//       }

    
//    else {
//     _solefront.set(false);
//     _soleback.set(false);
//   }



// }

    // }



    //Do this later for timer
if (_joystick.getRawAxis(3) < -0.8) {
  if (_joystick.getRawButton(8)) {
    
    launchingFinal = true;
  }

  else {
    launchingFinal = false;
  }


  while (launchingFinal == true) {



    if (timerRobo.get() < 3) {
      _soleback.set(true);
      middleSole = true;
    }
    if (timerRobo.get() > 3 && middleSole == true) {
      _solefront.set(true);
      middleSole = false;
    }
    if (timerRobo.get() > 5 && middleSole == false){
      _soleback.set(false);
      _solefront.set(false);
      timerRobo.stop();
      timerRobo.reset();
    }
    
  }



  
    // _soleback.set(true);
    //       System.out.println("1 UP");
    //       System.out.println(timerRobo.get());
    // if (timerRobo.get() > 3) {
    //   //_solenoid1_1.set(true);
    //   _solefront.set(true);
      
    //   System.out.println("2 UP");
    // }
    // if (timerRobo.get() > 5) {
    //   _solefront.set(false);
    //   _soleback.set(false);
    //     System.out.println("Boh dwn");
    //     timerRobo.stop();
    //     timerRobo.reset();
    // }
}

    

    if (_joystick.getRawButtonPressed(2)) {
      if (directionFlipped == false) {
        directionFlipped = true;
        System.out.println("DIRECTIONFLIPPED");
        System.out.println(directionFlipped);
      } else if (directionFlipped == true) {
        directionFlipped = false;
        
        System.out.println("DAMMIT");
      }
    }

    if (_joystick.getRawButtonPressed(10)) {
      // lifter.set
    }

    if (_joystick.getRawButtonPressed(9)) {
      // lifter.set-
    }

   
if (_joystick.getRawButton(12) && directionFlipped == false) {
_armLifter.set(ControlMode.PercentOutput, -0.6);
} 
else if (_joystick.getRawButton(11) && directionFlipped == false) {
  _armLifter.set(ControlMode.PercentOutput, 0.6);
}
else {
  _armLifter.set(ControlMode.PercentOutput, 0.0);
}



    if (_joystick.getRawButtonPressed(1) && directionFlipped == false) {
      // E.g. Operate Ball Thing
      // Talon motors on
    //   if (ballToggle == false) {
      
    // ballToggle = true;
    // }
    // else if (ballToggle == true) {
    //   ballToggle = false;
    // }
  
    
    
    
    } else if (_joystick.getRawButton(1) && directionFlipped == true) {
      // E.g. Operate Hatch Panel thing, which is solenoids\
      // Acivate these solenoids!
      // 2 grab 3 extend

      if (grabMay == false) {
        grabMay = true;
      } else if (grabMay == true) {
        grabMay = false;
      }

      _hatchGrabb2.set(true);

    } else if (!_joystick.getRawButton(1) && directionFlipped == true) {
      // E.g. Operate Hatch Panel thing, which is solenoids\
      // Acivate these solenoids!
      _hatchGrabb2.set(false);
      // _solenoid_hatch.set(false);
    } else if (!_joystick.getRawButtonPressed(1)) {
      // E.g. Operate Ball Thing
      // Talon motors off
      _ballGrabber.set(ControlMode.PercentOutput, 0.0);
    }


    if (_joystick.getRawButton(3) && directionFlipped == false) {
      _ballGrabber.set(ControlMode.PercentOutput, 1);
    }
    else if (_joystick.getRawButton(1) && directionFlipped == false) {
      _ballGrabber.set(ControlMode.PercentOutput, -1);
    }

    // For extend

    if (_joystick.getRawButton(3) && directionFlipped == true) {
      _hatchExt3.set(true);
    } else if (!_joystick.getRawButton(3) && directionFlipped == true) {
      _hatchExt3.set(false);
    }

    if (directionFlipped == false) {
      _joyforward = _joyforwardRaw * 1;
      _joyrotate = _joyrotateRaw * 1;
    } else if (directionFlipped == true) {

      _joyforward = _joyforwardRaw * -1;
      _joyrotate = _joyrotateRaw * 1;
    }

    
    
   if (_joystick.getRawButton(5)) {

        if (speedToggle == false) {
          speedToggle = true;
        }
        else if (speedToggle == true) {
          speedToggle = false;
        }

      }


      if (speedToggle == true){

      if ((_joyforward < -0.25 || _joyforward > 0.25) || (_joyrotate < -0.25 || _joyrotate > 0.25)) {
        _leftFrontCanSparkMax.set((_joyrotate - _joyforward) / 3);
        _leftBackCanSparkMax.set((_joyrotate - _joyforward) / 3);
        _rightFrontCanSparkMax.set((_joyrotate + _joyforward) / 3);
        _rightBackCanSparkMax.set((_joyrotate + _joyforward) / 3);
  
      } else {
        _leftFrontCanSparkMax.set(0);
        _rightFrontCanSparkMax.set(0);
        _leftBackCanSparkMax.set(0);
        _rightBackCanSparkMax.set(0);
      }
    }
    else {
      if ((_joyforward < -0.25 || _joyforward > 0.25) || (_joyrotate < -0.25 || _joyrotate > 0.25)) {
        _leftFrontCanSparkMax.set((_joyrotate - _joyforward));
        _leftBackCanSparkMax.set((_joyrotate - _joyforward));
        _rightFrontCanSparkMax.set((_joyrotate + _joyforward));
        _rightBackCanSparkMax.set((_joyrotate + _joyforward));
  
      } else {
        _leftFrontCanSparkMax.set(0);
        _rightFrontCanSparkMax.set(0);
        _leftBackCanSparkMax.set(0);
        _rightBackCanSparkMax.set(0);
      }
    
    }
  }

    // if (_joystick.getRawButtonPressed(11) == true) {
    // //If joystick is up

    // if (isToggled == true) {
    // isToggled = false;
    // fSolenoidRe();

    // }
    // else if (isToggled == false){
    // isToggled = true;
    // fSolenoidEx();
    // }

    // }

    // else if (_joystick.getY() > 0.25) {

    // //If Joystick is down

    // fSolenoidRe();

    // }

  

  /**
   * This function is called periodically during test mode.
   */

  @Override

  public void testInit() {
    directionFlipped = false;
    // double ledMode =
    // NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").getDouble(0);

    Update_Limelight_Tracking();
    ledEntry.setDouble(1);
    camMode.setDouble(1);
  }

  @Override
  public void testPeriodic() {

    double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);

    double _joyforwardRaw = _joystick.getY();
    double _joyrotateRaw = _joystick.getZ();

    // if (_joystick.getRawButtonPressed(3)) {

    // if (button11Toggle == false) {

    // button11Toggle = true;
    // _solenoid1_1.set(true);
    // }
    // else if (button11Toggle == true) {
    // button11Toggle = false;
    // _solenoid1_1.set(false);
    // }

    // // Solenoid _solenoid1_1 = new Solenoid(2, 4);

    // // Solenoid _solenoid1_2 = new Solenoid(2, 5);
    // }

    // if (_joystick.getRawButtonPressed(5)) {

    // if (button12Toggle == false) {

    // button12Toggle = true;
    // _solenoid1_2.set(true);
    // }
    // else if (button12Toggle == true) {
    // button12Toggle = false;
    // _solenoid1_2.set(false);
    // }

    // }

    if (_joystick.getRawButtonPressed(2)) {
      if (directionFlipped == false) {
        directionFlipped = true;
      } else if (directionFlipped == true) {
        directionFlipped = false;
      }

    }

    // This is to operate the arms, have fun

    if (_joystick.getRawButtonPressed(1) && directionFlipped == false) {
      // E.g. Operate Ball Thing
      // Talon motors on
    } else if (_joystick.getRawButtonPressed(1) && directionFlipped == true) {
      // E.g. Operate Hatch Panel thing, which is solenoids\
      // Acivate these solenoids!

      // _solenoid_hatch.set(true);
    }

    else if (!_joystick.getRawButtonPressed(1) && directionFlipped == true) {
      // E.g. Operate Hatch Panel thing, which is solenoids\
      // Acivate these solenoids!

      // _solenoid_hatch.set(false);
    } else if (!_joystick.getRawButtonPressed(1) && directionFlipped == false) {
      // E.g. Operate Ball Thing
      // Talon motors off
    }

    if (directionFlipped == false) {
      _joyforward = _joyforwardRaw * 1;
      _joyrotate = _joyrotateRaw * 1;
    } else if (directionFlipped == true) {

      _joyforward = _joyforwardRaw * -1;
      _joyrotate = _joyrotateRaw * 1;
    }

    if (!_joystick.getRawButton(1)) {

      if ((_joyforward < -0.25 || _joyforward > 0.25) || (_joyrotate < -0.25 || _joyrotate > 0.25)) {
        _leftFrontCanSparkMax.set((_joyrotate - _joyforward) / 3);
        _leftBackCanSparkMax.set((_joyrotate - _joyforward) / 3);
        _rightFrontCanSparkMax.set((_joyrotate + _joyforward) / 3);
        _rightBackCanSparkMax.set((_joyrotate + _joyforward) / 3);

      } else {
        _leftFrontCanSparkMax.set(0);
        _rightFrontCanSparkMax.set(0);
        _leftBackCanSparkMax.set(0);
        _rightBackCanSparkMax.set(0);
      }
    }

    else if (_joystick.getRawButton(1)) {

      if ((_joyforward < -0.25 || _joyforward > 0.25) || (_joyrotate < -0.25 || _joyrotate > 0.25)) {
        _leftFrontCanSparkMax.set((_joyrotate - _joyforward));
        _leftBackCanSparkMax.set((_joyrotate - _joyforward));
        _rightFrontCanSparkMax.set((_joyrotate + _joyforward));
        _rightBackCanSparkMax.set((_joyrotate + _joyforward));

      } else {
        _leftFrontCanSparkMax.set(0);
        _rightFrontCanSparkMax.set(0);
        _leftBackCanSparkMax.set(0);
        _rightBackCanSparkMax.set(0);
      }

    }

    else {
      _leftFrontCanSparkMax.set(0);
      _rightFrontCanSparkMax.set(0);
      _leftBackCanSparkMax.set(0);
      _rightBackCanSparkMax.set(0);
    }

    if (_joystick.getRawAxis(3) < -0.8) {

      fCompOn();

    }

    else {

      fCompOff();

    }

    // if (_joystick.getRawButtonPressed(11) == true) {
    // //If joystick is up

    // if (isToggled == true) {
    // isToggled = false;
    // fSolenoidRe();

    // }
    // else if (isToggled == false){
    // isToggled = true;
    // fSolenoidEx();
    // }

    // }

    // else if (_joystick.getY() > 0.25) {

    // //If Joystick is down

    // fSolenoidRe();

    // }

  }

  // This is our custom functions

  public void fCompOn() {

    _compressedor.setClosedLoopControl(true);
    _compressedor.start();
    // System.out.println("Compressor On");

  }

  public void fCompOff() {

    _compressedor.setClosedLoopControl(false);
    _compressedor.stop();
    // System.out.println("Compressor Off");

  }

  // public void fSolenoidRe() {

  // System.out.println("Retracting Solenoid");
  // _solenoid1_1.set(false);
  // }

  // public void fSolenoidEx() {

  // System.out.println("Extending Solenoid");
  // _solenoid1_1.set(true);

  // }

  public void Update_Limelight_Tracking() {
    // These numbers must be tuned for your Robot! Be careful!
    final double STEER_K = 0.02; // how hard to turn toward the target
    final double DRIVE_K = 0.26; // how hard to drive fwd toward the target
    final double DESIRED_TARGET_AREA = 13.0; // Area of the target when the robot reaches the wall
    final double MAX_DRIVE = 0.7; // Simple speed limit so we don't drive too fast

    double tv = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
    double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
    double ty = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0);
    double ta = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ta").getDouble(0);
    camMode = NetworkTableInstance.getDefault().getTable("limelight").getEntry("camMode");
    ledEntry = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode");
    // ty = ty - 8.00;
    ta = ta - 13.00;
    tx = tx + 6.4;

    if (tv != 1.0) {
      m_LimelightHasValidTarget = false;
      m_LimelightDriveCommand = 0.0;
      m_LimelightSteerCommand = 0.0;
      return;
    } else if (tv == 1.0) {

      m_LimelightHasValidTarget = true;

    }

    // Start with proportional steering
    double steer_cmd = tx * STEER_K;
    m_LimelightSteerCommand = steer_cmd;

    // try to drive forward until the target area reaches our desired area
    // double drive_cmd = (DESIRED_TARGET_AREA - ta) * DRIVE_K;
    double drive_cmd = ta * DRIVE_K;

    // don't let the robot drive too fast into the goal
    // if (drive_cmd > MAX_DRIVE)
    // {
    // drive_cmd = MAX_DRIVE;
    // }
    m_LimelightDriveCommand = drive_cmd / 5;

    _tavar = ta;
  }

  public void limelightAutonomous() {
    Update_Limelight_Tracking();
  
      double steer = _joystick.getZ();
      double drive = _joystick.getY();
      boolean auto = _joystick.getRawButton(9);
  
      steer *= 1;
      drive *= -1;
  
      if (!auto) {
        if (m_LimelightHasValidTarget) {
          m_Drive.arcadeDrive(-m_LimelightDriveCommand, m_LimelightSteerCommand);
          System.out.println(-m_LimelightDriveCommand);
  
          System.out.println(-m_LimelightSteerCommand);
        }
  
        else if (_tavar > 3.0) {
          _leftFrontCanSparkMax.set(-0.3);
          _rightFrontCanSparkMax.set(-0.3);
          _leftBackCanSparkMax.set(-0.3);
          _rightBackCanSparkMax.set(-0.3);
  
        } else {
          m_Drive.arcadeDrive(0.0, 0.0);
        }
  
      } else {
        m_Drive.arcadeDrive(drive / 2, steer / 2);
      }
  
  }
  


}






