/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;



import javax.swing.JToggleButton;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Solenoid;
import io.github.pseudoresonance.pixy2api.Pixy2;
import io.github.pseudoresonance.pixy2api.Pixy2.LinkType;
import io.github.pseudoresonance.pixy2api.links.Link;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */


 






public class Robot extends IterativeRobot {


     Pixy2 _pixyCam = Pixy2.createInstance(LinkType.UART);

     

     

     
 

  
  @Override
  public void robotInit() {
   
   


    _pixyCam.init();













  }

 
  @Override
  public void robotPeriodic() {


            
  }

  
  @Override
  public void autonomousInit() {
 
  }

  
  @Override
  public void autonomousPeriodic() {
    
    
  }

 
  @Override
  public void teleopInit() {

    

          

 
    
      

    

      

      
    


  }


  @Override
  public void teleopPeriodic() {


 
    

  }


  
  @Override
  public void testPeriodic() {

   

  }

  

 
}
